#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Declaracion de Array de Caracteres, ejemplo 128 caracteres
    char texto[128];

    //Solicitud de un Array de Caracteres al usuario sin el uso del operador "&"
    printf("Ingrese su nombre: ");
    scanf("%s", texto);

    //Muestra de la concatenacion de caracteres del saludo en pantalla
    printf("Hola %s\n", texto);
    return 0;
}
