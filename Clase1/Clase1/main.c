#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Declaracion de variables
    float valorA;
    float valorB;
    float resultado;

    //Asignacion de variables mediante el metodo scanf()
    printf("Ingrese el valor A: ");
    scanf("%f", &valorA);

    printf("Ingrese el valor B: ");
    scanf("%f", &valorB);

    //Operacion aritmetica de suma de las variables A y B, y asignacion en Resultado.
    resultado = valorA + valorB;

    //Impresion en pantalla y concatenacion del resultado
    printf("El resultado es: %f\n", resultado);

    //Retorno de la funcion principal int main()
    return 0;
}
