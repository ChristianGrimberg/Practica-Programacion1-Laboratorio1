#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE* punteroArchivo;
    char rutaArchivo[] = "./Carpeta/Archivo.csv";
    int datosEscribir[5];
    int tamano;

    /**< Hardcode de datos */
    datosEscribir[0] = 4;
    datosEscribir[1] = 1;
    datosEscribir[2] = 33;
    datosEscribir[3] = -2;
    datosEscribir[4] = 45;

    punteroArchivo = fopen(rutaArchivo, "r");

    if(punteroArchivo==NULL)
    {
        punteroArchivo = fopen(rutaArchivo, "w");
        if(punteroArchivo!=NULL)
        {
            //tamano = fwrite(datosEscribir, sizeof(datosEscribir), 1, punteroArchivo);
            int i;
            for(i=0; i<5; i++)
            {
                if(i<4)
                {
                    tamano = fprintf(punteroArchivo, "%d,", datosEscribir[i]);
                }
                else
                {
                    tamano = fprintf(punteroArchivo, "%d", datosEscribir[i]);
                }
            }

            printf("Tamano del archivo: %d.\n", tamano);
        }
    }
    else
    {
        printf("Modo de lectura.\n");
    }

    /**< Cierre del archivo */
    if(fclose(punteroArchivo)==-1)
        printf("Error de cierre del archivo.\n");
    else
        printf("Se cerro el archivo.\n");

    /**< Lectura del archivo */

    return 0;
}
