#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE* archivoEscritura;
    FILE* archivoLectura;
    char rutaArchivo[] = "./Carpeta/ArchivoEjemplo.bin";
    int tamanoEscritura;
    int tamanoLectura;
    int datosEscribir[5];
    int datosLeidos[5];

    /**< Modo de apertura del archivo */
    archivoEscritura=fopen(rutaArchivo, "rb");
    if(archivoEscritura==NULL)
    {
        archivoEscritura=fopen(rutaArchivo, "wb");
        if(archivoEscritura==NULL)
        {
            printf("Error de apertura del archivo.\n");
        }
        else
        {
            printf("Modo escritura.\n");
        }
    }
    else
    {
        printf("Modo solo lectura.\n");
    }

    /**< Escritura del archivo */
    datosEscribir[0]=55;
    datosEscribir[1]=2;
    datosEscribir[2]=3;
    datosEscribir[3]=-1;
    datosEscribir[4]=10;

    tamanoEscritura = fwrite(datosEscribir, sizeof(datosEscribir), 1, archivoEscritura);

    if(tamanoEscritura==1)
    {
        printf("Se escribieron %d numeros.\n", tamanoEscritura);
    }
    else
    {
        printf("Error de escritura\nTamano:%d - sizeOf:%ld\n", tamanoEscritura, sizeof(datosEscribir));
    }


    /**< Cierre del archivo */
    if(fclose(archivoEscritura)==0)
    {
        printf("Se cerro el archivo.\n");
    }
    else
    {
        printf("Error de cierre del archivo.\n");
    }

    /**< Lectura del archivo */
    archivoLectura = fopen(rutaArchivo, "rb");
    if(archivoLectura==NULL)
    {
        printf("Error de apertura del archivo");
    }
    else
    {
        while(!feof(archivoLectura))
        {
            tamanoLectura = fread(datosLeidos, sizeof(datosLeidos), 1, archivoLectura);
            printf("Se leyó %d registros.\n", tamanoLectura);
        }
    }

    /**< Cierre del archivo */
    if(fclose(archivoLectura)==0)
    {
        printf("Se cerro el archivo.\n");
    }
    else
    {
        printf("Error de cierre del archivo.\n");
    }

    int i;
    for(i=0; i<5; i++)
    {
        printf("Valor: %d.\n", datosLeidos[i]);
    }

    return 0;
}
