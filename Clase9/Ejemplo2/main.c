#include <stdio.h>
#include <stdlib.h>
#define EMPLEADOS 3
#define VECTOR 31

struct sEmpleado
{
    int legajo;
    float salario;
    char nombre[VECTOR];
};

int main()
{
    struct sEmpleado empleados[EMPLEADOS], auxiliar;
    int i, j, k, l;

    /**< Se cargan los empleados y sus datos */
    for (k = 0; k < EMPLEADOS; k++)
    {
        printf("->Empleado %d<-\n", k+1);
        printf("Ingrese el nombre: ");
        fflush(stdin);
        scanf("%[^\n]", empleados[k].nombre);
        printf("Ingrese el legajo: ");
        scanf("%d", &empleados[k].legajo);
        printf("Ingrese el salario: ");
        scanf("%f", &empleados[k].salario);
    }

    /**< Burbugeo sobre estructuras: ejemplo de ordenamiento por salario */
    for (i = 0; i < EMPLEADOS-1; i++)
    {
        for (j = i+1; j < EMPLEADOS; j++)
        {
            if (empleados[i].salario > empleados[j].salario)
            {
                auxiliar = empleados[i];
                empleados[i] = empleados[j];
                empleados[j] = auxiliar;
            }
        }
    }

    printf("---------------------------\n");

    /**< Se muestran los valores del array secuencialmente */
    for (l = 0; l < EMPLEADOS; l++)
    {
        printf("El empleado es %s de legajo %d y salario %.2f\n", empleados[l].nombre, empleados[l].legajo, empleados[l].salario);
    }

    return 0;
}
