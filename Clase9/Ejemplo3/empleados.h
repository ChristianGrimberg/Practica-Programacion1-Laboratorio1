#ifndef EMPLEADOS_H_INCLUDED
#define EMPLEADOS_H_INCLUDED
#define NOMBRES 31
#define EMPLEADOS 5

typedef struct
{
    int dia;
    int mes;
    int anio;
} sFecha;

typedef struct
{
    int legajo;
    char nombre[NOMBRES];
    sFecha fechaNacimiento;
    float salario;
} sEmpleado;

void newEmployee(sEmpleado empleado[], int cantEmpleados);

void orderEmployees(sEmpleado empleado[], int cantEmpleados);

void listEmployees(sEmpleado empleado[], int cantEmpleados);

#endif
