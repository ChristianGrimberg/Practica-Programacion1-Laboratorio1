#include <stdio.h>
#include <stdlib.h>
#include "empleados.h"

int main()
{
    sEmpleado empleadoEmpresa[EMPLEADOS];

    /**< Se pide informacion de los Empleados */
    newEmployee(empleadoEmpresa, EMPLEADOS);

    printf("-------------------\n");

    /**< Se muestra la informacion de los Empleados */
    listEmployees(empleadoEmpresa, EMPLEADOS);

    return 0;
}
