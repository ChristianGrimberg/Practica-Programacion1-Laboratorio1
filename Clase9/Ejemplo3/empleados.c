#include <stdio.h>
#include <stdlib.h>
#include "empleados.h"

void newEmployee(sEmpleado empleado[], int cantEmpleados)
{
    int i;
    for (i=0; i<cantEmpleados; i++)
    {
        printf("->Empleado %d-<\n", i+1);
        printf("Ingrese el nombre: ");
        fflush(stdin);
        scanf("%[^\n]", empleado[i].nombre);
        printf("Ingrese el legajo: ");
        scanf("%d", &empleado[i].legajo);
        printf("Ingrese el dia de nacimiento: ");
        scanf("%d", &empleado[i].fechaNacimiento.dia);
        printf("Ingrese el mes de nacimiento: ");
        scanf("%d", &empleado[i].fechaNacimiento.mes);
        printf("Ingrese el anio de nacimiento: ");
        scanf("%d", &empleado[i].fechaNacimiento.anio);
        printf("Ingrese el salario: ");
        scanf("%f", &empleado[i].salario);
    }
}

void orderEmployees(sEmpleado empleado[], int cantEmpleados)
{

}

void listEmployees(sEmpleado empleado[], int cantEmpleados)
{
    int i;

    for (i=0; i<EMPLEADOS; i++)
    {
        printf("El empleado %s de legajo %d, nacido en %d/%d/%d, tiene un sueldo de %.2f\n"
               , empleado[i].nombre
               , empleado[i].legajo
               , empleado[i].fechaNacimiento.dia
               , empleado[i].fechaNacimiento.mes
               , empleado[i].fechaNacimiento.anio
               , empleado[i].salario);
    }
}
