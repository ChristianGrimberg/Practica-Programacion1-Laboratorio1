#include <stdio.h>
#include <stdlib.h>
#define PERSONA 20

struct fecha
{
    int dia;
    int mes;
    int anio;
};

struct gente
{
    char nombre[PERSONA];
    struct fecha fechaNacimiento;
};

int main()
{
    struct gente persona;
    struct fecha nacimiento;

    printf("Ingrese el nombre: ");
    fflush(stdin);
    scanf("%[^\n]", persona.nombre);
    printf("Ingrese el dia: ");
    scanf("%d", &nacimiento.dia);
    printf("Ingrese el mes: ");
    scanf("%d", &nacimiento.mes);
    printf("Ingrese el anio: ");
    scanf("%d", &nacimiento.anio);

    persona.fechaNacimiento = nacimiento;

    printf("El nombre es %s y el nacimiento es %d/%d/%d", persona.nombre, persona.fechaNacimiento.dia, persona.fechaNacimiento.mes, persona.fechaNacimiento.anio);

    return 0;
}
