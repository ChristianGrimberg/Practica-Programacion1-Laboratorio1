#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "productos.h"

void ingresarNuevoProducto(Producto producto[], int cantidadProducto)
{
    system("cls");
    char confirmar = 's';
    int contador = 0;

    do
    {
        printf("===========================================================\n");
        printf("Ingrese el codigo del producto %d: ", contador+1);
        scanf("%d", &producto[contador].IdProducto);
        printf("ingrese la descripcion del producto %d: ", contador+1);
        fflush(stdin);
        scanf("%s", producto[contador].descripcionProducto);
        printf("ingrese el importe del producto %d: ", contador+1);
        scanf("%f", &producto[contador].importeProducto);
        printf("ingrese la cantidad que hay del producto %d: ", contador+1);
        scanf("%d", &producto[contador].cantidadProducto);
        printf("ingrese si esta disponible el producto %d (1 para si / 0 para no): ", contador+1);
        scanf("%d", &producto[contador].estadoProducto);
        printf("===========================================================\n");

        printf("Desea agregar un nuevo Producto? (s/n): ");
        fflush(stdin);
        scanf("%c", &confirmar);
        confirmar = tolower(confirmar);
        contador++;
    } while (confirmar == 's' && contador < cantidadProducto);
}

void imprimirListadoProveedores(Proveedor proveedores[], int cantidadProveedores)
{
    printf("================================================\n");
    printf("[Id]\t[Proveedor]\t\t\t[Estado]\n");
    printf("------------------------------------------------\n");

    int i;
    for (i=0; i<cantidadProveedores; i++)
    {
        printf("%d\t%s\t\t\t%d\n", proveedores[i].IdProveedor, proveedores[i].descripcionProveedor, proveedores[i].estadoProveedor);
    }

    printf("================================================\n");
}
