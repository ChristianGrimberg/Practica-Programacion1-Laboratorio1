#include <stdio.h>
#include <stdlib.h>
#include "productos.h"
#include "menu.h"

int main()
{
    Producto producto[CANTIDAD];
    Proveedor proveedores[] = {{1, "Proveedor 1", 1}, {2, "Proveedor 2", 1}, {3, "Proveedor 3", 1}};
    int opcion;
    char confirmar = 's';

    while (confirmar == 's')
    {
        system("cls");

        menu();

        printf("Valor elegido: ");
        scanf("%d",&opcion);

        switch(opcion)
        {
            case 1:
                ingresarNuevoProducto(producto, CANTIDAD);
                break;
            case 2:
                imprimirListadoProveedores(proveedores, 3);
                break;
            case 6:
                confirmar = 'n';
                break;
        }

        system("pause");
    }

    return 0;
}
