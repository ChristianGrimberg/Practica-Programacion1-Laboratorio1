#include <stdio.h>
#include <stdlib.h>
#include "menu.h"

void menu(void)
{
    /**< Menu de opciones */
    printf("===========================================================\n");
    printf("Bienvenido, ingrese el numero del menu que desea ingresar:\n");
    printf(" 1. Ingresar productos.\n");
    printf(" 2. Imprimir lista de proveedores.\n");
    printf(" 3. Imprimir lista de productos.\n");
    printf(" 4. Imprimir lista de productos por proveedor.\n");
    printf(" 5. Imprimir lista de proveedores y productos.\n");
    printf(" 6. Salir del programa.\n");
    printf("===========================================================\n");
}
