#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED
#define DESCRIPCION 51
#define CANTIDAD 3

typedef struct
{
    int IdProveedor;
    char descripcionProveedor[DESCRIPCION];
    int estadoProveedor;
} Proveedor;

typedef struct
{
    int IdProducto;
    char descripcionProducto[DESCRIPCION];
    float importeProducto;
    int cantidadProducto;
    int estadoProducto;
} Producto;

typedef struct
{
    int IdProducto;
    int IdProveedor;
} ProductoPorProveedor;

void ingresarNuevoProducto(Producto producto[], int cantidadProducto);

void imprimirListadoProveedores(Proveedor proveedores[], int cantidadProveedores);


#endif // FUNCIONES_H_INCLUDED
