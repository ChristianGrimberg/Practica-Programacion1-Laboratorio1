#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct
{
    int edad;
    char nombre[20];
} Persona;

void preguntarNombre(char nombre[]);

int preguntarEdad();

char preguntarSalir();

void inicializar(Persona personas[], int sizeOfArrayOfPersonas);

void listar(Persona personas[], int sizeOfArrayOfPersonas);

int main()
{
    int size = 10;
    int index=0;
    Persona* * lista = (Persona* * )malloc(sizeof(Persona * )*size);

    do
    {
        /*
        Persona persona;
        preguntarNombre(&persona->nombre);
        persona.edad = preguntarEdad();
        lista[index] = & persona; // Ver explicacion sobre esta línea
        */

        Persona* persona = (Persona*)malloc(sizeof(Persona));
        preguntarNombre(persona->nombre);
        persona->edad = preguntarEdad();
        lista[index] = persona;
        index++;

        if(index>=size)
        {
            // incrementamos el tamaño del array
            size+=10;
            lista = realloc(lista,sizeof(Persona*)*size);
        }
    } while(preguntarSalir()!='S');

    inicializar(*lista, size); //Se debe inicializar con otro vector de estructuras antes
    listar(*lista, size);

    getchar();

    return 0;
}

void preguntarNombre(char nombre[])
{
    char nombreAux[20];

    printf("Ingrese el nombre: ");
    setbuf(stdin, NULL);
    scanf("%s", nombreAux);

    strcpy(nombre, nombreAux);
}

int preguntarEdad()
{
    int enteroAux;

    printf("Ingrese la edad: ");
    scanf("%d", &enteroAux);

    return enteroAux;
}

char preguntarSalir()
{
    char preguntarAux;

    printf("Desea Salir (S/N)?: ");
    setbuf(stdin, NULL);
    scanf("%c", &preguntarAux);

    preguntarAux = toupper(preguntarAux);

    return preguntarAux;
}

void inicializar(Persona personas[], int sizeOfArrayOfPersonas)
{
    int i;
    for(i=0; i<sizeOfArrayOfPersonas ;i++)
    {
        strcpy(personas[i].nombre,"");
        personas[i].edad = 0;
    }

}

void listar(Persona personas[], int sizeOfArrayOfPersonas)
{
    int i;
    for(i=0; i<sizeOfArrayOfPersonas ;i++)
    {
        printf("%s - %d\n", personas[i].nombre, personas[i].edad);
    }
}
