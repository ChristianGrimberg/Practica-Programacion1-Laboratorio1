# Prácticas de Programacion 1 y Laboratorio 1
## Clase 1 - 19/MAR/2018 (Laboratorio 1)
### [1. Diferencias entre Lenguajes Javascript y ANSI C](https://github.com/codeutnfra/programacion_1_laboratorio_1/blob/master/Clase_01/apuntes/Diferencias_JS_y_C.pdf)
* Variables de Tipo de Datos
* Operadores Aritméticos Básicos
* Salida y entrada de datos en Consola

### [2. Introducción al Lenguaje C](https://github.com/codeutnfra/programacion_1_laboratorio_1/blob/master/Clase_01/apuntes/Introducci%C3%B3n_a_C.pdf)

## Clase 2 - 20/MAR/2018 (Programación 1)
### [3. Primeros Algoritmos](https://github.com/codeutnfra/programacion_1_laboratorio_1/blob/master/Clase_02/apuntes/Primeros_algoritmos.pdf)
* Iteradores FOR, WHILE, DO...WHILE
* Estructuras de Control IF, SWITCH
* Acumuladores, Contadores, Máximos y Mínimos