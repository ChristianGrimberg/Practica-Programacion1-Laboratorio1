#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char nombre[31];

    printf("Ingrese el nombre: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]", nombre); /**< Uso con expresion regular que toma el ingreso del usuario hasta que presione Enter (incluye espacios) */

    //gets(nombre);

    /**< Para pasar cadenas a mayusculas o minusculas: ¡Atencion! Trabaja por referencia */
    //strupr(cadena);
    //strlwr(cadena);

    /**< Para palabras con mayusculas y minusculas */
    //strlwr(cadena);
    //cadena[0] = toupper(cadena[0]);

    /**< Concatenar strings ¡Atencion! Trabaja por referencia */
    //char nombre[] = "Juan";
    //char apellido[] = "Lopez";
    //strcat(nombre, " ");
    //strcat(nombre, apellido);

    printf("Hola %s", nombre);

    return 0;
}
