#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/**< ver #define ESC 27 (para salir del programa) con getch() (Para determinar que haya tomado la tecla ESC) */
/**< ver system("cls") para el TP1 */

int main()
{
    char nombre[31];
    char apellido[31];

    printf("Ingrese el nombre: ");
    setbuf(stdin, NULL);
    scanf("%s[^\n]", nombre);

    printf("Ingrese el apellido: ");
    setbuf(stdin, NULL);
    scanf("%s[^\n]", apellido);

    /**< Ver la funcion fgets() */

    strlwr(nombre);
    strlwr(apellido);
    nombre[0] = toupper(nombre[0]);
    apellido[0] = toupper(apellido[0]);

    strcat(nombre, " ");
    strcat(nombre, apellido);

    printf("Hola %s", nombre);

    return 0;
}
