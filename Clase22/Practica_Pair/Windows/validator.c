#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <ctype.h>
#include "validator.h"
#include "programMenu.h"

void clearBuffer(void)
{
    char memoryBuffer = '\n';

    /**< Teniendo en cuenta que si queda basura en memoria de la carga del usuario que el programa no proceso */
    while (getchar() != memoryBuffer) /**< La condicion de iteracion pide un valor de teclado hasta que el usuario presiono Enter */
    {
        /**< Sin implementacion, la condicion del while ya limpia la memoria */
    }
}

int isNumber(char value[])
{
   int returnValue = 1;
   int i=0;

   while(value[i] != '\0')
   {
       if(value[i] < '0' || value[i] > '9')
            returnValue = 0;
       i++;
   }

   return returnValue;
}

int isOnlyLetters(char value[])
{
   int returnValue = 1;
   int i=0;

   while(value[i] != '\0')
   {
       if((value[i] != ' ')
        && (value[i] < 'a' || value[i] > 'z')
        && (value[i] < 'A' || value[i] > 'Z'))
        {
            returnValue = 0;
            break;
        }

       i++;
   }

   return returnValue;
}

float requestFloatNumber(char message[])
{
    float floatAux;
    printf("%s",message);
    scanf("%f",&floatAux);
    return floatAux;
}

int requestIntNumber(char message[])
{
    int intAux;
    printf("%s",message);
    scanf("%d",&intAux);
    return intAux;
}

char requestCharValue(char message[])
{
    char charAux;
    printf("%s",message);
    setbuf(stdin, NULL); /**< Se limpia el buffer de entrada del teclado para los distintos SO */
    scanf("%c",&charAux);
    return charAux;
}

char* requestStringValue(char message[])
{
    char stringValue[STRING_MAX];

    printf("%s",message);
    scanf("%[^\n]", stringValue);

    return stringValue;
}

int requestNumberInRange(int minNumber, int maxNumber)
{
    int userOption = 0; /**< Variable entera utilizada para almacenar la seleccion del menu del usuario */
    int numberOption; /**< Variable utilizada para validar el ingreso del usuario */

    do /**< Iterador que solicita al usuario el ingreso de valores enteros del 1 al 9 */
    {
        printf("Ingrese un valor del [%d al %d]: ", minNumber, maxNumber);
        //setbuf(stdin, NULL); /**< Se limpia el buffer de entrada del teclado para los distintos SO */
        numberOption = scanf("%d", &userOption); /**< Se valida si el usuario ingresa un dato numerico o no */
        clearBuffer(); /**< Se limpia el buffer de memoria de carga de datos incorrectos */
    } while (!numberOption || (userOption < minNumber || userOption > maxNumber));

    return userOption;
}

int setLimitsOnNumber(double numberToLimit, int numericTypeIndicator)
{
    int overflowError; /**< Variable que almacena la evaluacion de la estructura de seleccion en funcion de los limites definidos */

    /**< Si el indicador de tipo de dato a evaluar es entero, y supera los limites de los enteros, y es distinto a cero */
    if (numericTypeIndicator == 0 && (numberToLimit >= INT_MAX || numberToLimit <= INT_MIN) && numberToLimit != 0)
    {
        overflowError = 0; /**< El numero supera los limites de los enteros */
    }
    else
    {
        /**< Si el indicador de tipo de dato a evaluar es flotante y supera los limites de los flotantes, excluyendo al cero */
        if (numericTypeIndicator == 1
            && (numberToLimit >= FLT_MAX || (numberToLimit <= FLT_MIN && numberToLimit >= -FLT_MIN) || numberToLimit <= -FLT_MAX)
             && numberToLimit != 0)
        {
                overflowError = 2; /**< El numero supera los limites de los flotantes */
        }
        else /**< Si el numero esta dentro de los limites de los enteros o flotantes incluyendo al cero */
        {
            overflowError = 1; /**< El numero esta dentro de los limites de los enteros y de los flotantes */
        }
    }

    return overflowError;
}

int stringCompareWithLowerCase(char string1[], char string2[])
{
    int returnValue = 0;
    int i;

    i=0;
    while(string1[i]!='\0')
    {
        string1[i] = tolower(string1[i]);
        i++;
    }

    i=0;
    while(string2[i]!='\0')
    {
        string2[i] = tolower(string2[i]);
        i++;
    }

    if(strcmp(string1, string2)==0)
    {
        returnValue = 1;
    }

    return returnValue;
}

int stringBooleanToInteger(char boolean[])
{
    int returnValue = -1;

    if(strcmp(boolean, "true")==0)
        returnValue = 1;
    else if (strcmp(boolean, "false")==0)
        returnValue = 0;

    return returnValue;
}
