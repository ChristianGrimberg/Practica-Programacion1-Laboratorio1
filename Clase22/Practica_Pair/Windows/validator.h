#ifndef VALIDATOR_H_INCLUDED
#define VALIDATOR_H_INCLUDED
#define STRING_DEFAULT 51
#define STRING_MAX 256

/** \brief
 * Borrado de caracteres en memoria de un ingreso no controlado de datos del usuario hasta que presiono Enter.
 * \param void No se requiere parametros.
 * \return No retorna valores solo limpia la memoria de caracteres no procesados por el programa.
 *
 */
void clearBuffer(void);

/** \brief
 * Verifica si el valor recibido es numerico.
 * \param value Valor a ser analizado.
 * \return 1 si es numerico y 0 si no lo es.
 *
 */
int isNumber(char value[]);

/** \brief
 * Verifica si el valor recibido contiene solo letras.
 * \param value Array con la cadena a ser analizada.
 * \return 1 si contiene solo ' ' y letras y 0 si no lo es.
 *
 */
int isOnlyLetters(char value[]);

/** \brief
 * Solicita un numero al usuario y devuelve el resultado.
 * \param message Es el mensaje a ser mostrado.
 * \return El numero ingresado por el usuario.
 *
 */
float requestFloatNumber(char message[]);

/** \brief
 * Solicita un numero al usuario y devuelve el resultado.
 * \param message Es el mensaje a ser mostrado.
 * \return El numero ingresado por el usuario.
 *
 */
int requestIntNumber(char message[]);

/** \brief
 * Solicita un caracter al usuario y devuelve el resultado.
 * \param mensaje Es el mensaje a ser mostrado.
 * \return El caracter ingresado por el usuario.
 *
 */
char requestCharValue(char message[]);

/** \brief
 * Solicita al usuario el ingreso de una cadena de caracteres y la valida.
 * \param mensaje Es el mensaje a ser mostrado.
 * \return El puntero al inicio de la cadena de caracteres ingresada por el usuario.
 *
 */
char* requestStringValue(char message[]);

/** \brief
 * Solicita al usuario el ingreso de un numero de un rango definido y lo valida.
 * \param minNumber El numero minimo del rango que debe ingresar el usuario.
 * \param maxNumber El numero maximo del rango que debe ingresar el usuario.
 * \return El numero entero validado en el rango definido.
 *
 */
int requestNumberInRange(int minNumber, int maxNumber);

/** \brief
 * Funcion que determina si el numero se puede calcular dentro de los numeros enteros o flotantes.
 * \param numberToLimit Se debe ingresar un numero a evaluar.
 * \param numericTypeIndicator Se debe indicar con 0 si el numero a evaluar es entero y 1 si el numero a evaluar es flotante.
 * \return
 * Devuelve 0 si el numero supero los limites de los enteros,
 * 2 si el numero supero los limites de los flotantes
 * y 1 si esta dentro de los limites de flotantes o enteros.
 *
 */
int setLimitsOnNumber(double numberToLimit, int numericTypeIndicator);

/** \brief
 * Funcion que compara el valor de las dos cadenas ingreadas convirtiendolas a minusculas
 * \param string1[] char Valor de la cadena uno
 * \param string2[] char Valor de la cadena dos
 * \return Retorna 1 si las cadenas en minusculas son iguales y 0 si son diferentes.
 *
 */
int stringCompareWithLowerCase(char string1[], char string2[]);

/** \brief
 * Funcion que convierte un valor de cadena booleano true o false a entero
 * \param boolean[] char Cadena true o false
 * \return Retorna 1 si la cadena es true, 0 si es false, y -1 si no es ninguna de las dos cadenas.
 *
 */
int stringBooleanToInteger(char boolean[]);

#endif // VALIDATOR_H_INCLUDED
