#include <stdio.h>
#include <stdlib.h>
#include "ArrayList.h"
#include "Employee.h"
#include "parser.h"
#include "validator.h"

int parserEmployee(FILE* pFile , ArrayList* pArrayListEmployee)
{
    int returnValue = 0;
    //int counter = 0;
    /**< Variables estaticas para operar los valores obtenidos en cada linea del archivo */
    char idAux[51];
    char nameAux[51];
    char lastNameAux[51];
    char isEmptyAux[51];

    Employee* empleadoAux;

    while(!feof(pFile))
    {
        fscanf(pFile, "%[^,],%[^,],%[^,],%[^\n]\n", idAux, nameAux, lastNameAux, isEmptyAux);

        if(isNumber(idAux))
        {
            empleadoAux = employee_new();
            if(employee_setId(empleadoAux, atoi(idAux)))
            {
                if(employee_setName(empleadoAux, nameAux)
                   && employee_setLastName(empleadoAux, lastNameAux)
                   && employee_setIsEmpty(empleadoAux, stringBooleanToInteger(isEmptyAux)))
                {
                    if(pArrayListEmployee->add(pArrayListEmployee, empleadoAux)==0)
                    {
                        returnValue++;
                    }
                }
            }
        }
        else
            continue;
    }

    fclose(pFile);

    return returnValue;
}
