#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ArrayList.h"
#include "Employee.h"
#include "validator.h"

int employee_compare(void* pEmployeeA,void* pEmployeeB)
{
    return 0;
}


void employee_print(Employee* this)
{
    char stringBoolean[51];

    if(employee_getIsEmpty(this))
        strcpy(stringBoolean, "true");
    else if(employee_getIsEmpty(this)==0)
        strcpy(stringBoolean, "false");

    /**< Impresion en pantalla de los datos del empleado */
    if(employee_getId(this)!=ERROR_VALUE)
        printf("%d\t%s %s\t%s\n", employee_getId(this), employee_getName(this), employee_getLastName(this), stringBoolean);
}


Employee* employee_new(void)
{
    Employee* employeeAux = NULL;

    /**< Se reserva un nuevo espacio de memoria para almacenar el Empleado */
    employeeAux = (Employee*)malloc(sizeof(Employee));

    if(employeeAux==NULL)
    {
        printf("No hay espacio en memoria para un nuevo Empleado.\n");
    }

    return employeeAux;

}

void employee_delete(Employee* this)
{


}

int employee_setId(Employee* this, int id)
{
    int returnValue = 0;
    char numberChar[51];

    //itoa(id, numberChar, 10);
    sprintf(numberChar, "%d", id);  /**< Conversion de enteros a cadena en todos los SO */

    if(isNumber(numberChar))
    {
        this->id = id;

        returnValue = 1;
    }

    return returnValue;

}

int employee_getId(Employee* this)
{
    int returnValue = ERROR_VALUE;
    char numberChar[51];

    //itoa(this->id, numberChar, 10);
    sprintf(numberChar, "%d", this->id); /**< Conversion de enteros a cadena en todos los SO */

    if(isNumber(numberChar))
        returnValue = this->id;

    return returnValue;
}

int employee_setName(Employee* this, char name[])
{
    int returnValue = 0;

    if(strlen(name)<=STRING_DEFAULT)
    {
        strcpy(this->name, name);
        returnValue = 1;
    }

    return returnValue;
}

char* employee_getName(Employee* this)
{
    return this->name;
}

int employee_setLastName(Employee* this, char lastName[])
{
    int returnValue = 0;

    if(strlen(lastName)<=STRING_DEFAULT)
    {
        strcpy(this->lastName, lastName);
        returnValue = 1;
    }

    return returnValue;
}

char* employee_getLastName(Employee* this)
{
    return this->lastName;
}

int employee_setIsEmpty(Employee* this, int isEmpty)
{
    int returnValue = 0;

    if(isEmpty || isEmpty==0)
    {
        this->isEmpty=isEmpty;
        returnValue = 1;
    }

    return returnValue;
}

int employee_getIsEmpty(Employee* this)
{
    return this->isEmpty;
}
