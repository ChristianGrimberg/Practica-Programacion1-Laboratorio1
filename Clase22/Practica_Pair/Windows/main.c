#include <stdio.h>
#include <stdlib.h>
#include "ArrayList.h"
#include "Employee.h"
#include "programMenu.h"
#include "validator.h"
#include "parser.h"
#define ARCHIVE "data.csv"

/****************************************************
    Menu:
        1. Parse del archivo data.csv
        2. Listar Empleados
        3. Ordenar por nombre
        4. Agregar un elemento
        5. Elimina un elemento
        6. Listar Empleados (Desde/Hasta)
*****************************************************/


int main()
{
    int opcion; /**< Variable para almacenar la opcion elegida por el usuario */
    int i = 0;
    char seguir = 's';
    ArrayList* lista = al_newArrayList();
    int qtyEmployees = 0;
    Employee* employee;
    FILE * pFile;

    while(seguir == 's') /**< Se itera el menu en consola mientras que no se elija una opcion diferente a la de entrada */
    {
        /**< Se valida que haya memoria suficiente para trabajar con el ArrayList en la carga inicial del programa */
        if(lista == NULL)
        {
            printf("No hay memoria suficiente para ejecutar la aplicacion.\n");
            seguir = 'n';
            continue;
        }

        system("cls"); /**< Se limpia la pantalla antes de la carga del menu de opciones */
        opcion = mainMenu(); /**< Se invoca a la funcion que muestra el menu en consola con los operandos ingresados por el usuario */

        switch(opcion) /**< Se operan con las opciones enteras del 1 al 9 por el menu en pantalla */
        {
            case 1:
                pFile = fopen (ARCHIVE, "r"); /**< Se opera con el archivo definido */
                qtyEmployees = parserEmployee(pFile, lista);
                if(qtyEmployees>0)
                    printf("Se cargaron %d empleados.\n", qtyEmployees);
                break;
            case 2:
                printf("ID\tEmpleado\t\tEsta vacio\n");
                for(i=0; i<qtyEmployees; i++)
                {
                    if(i%100==0 && i!=0)
                        pauseScreen();
                    employee = (Employee*)lista->get(lista, i);
                    employee_print(employee);
                }
                break;
            case 3:
                break;
            case 4:
                employee = employee_new();
                if(employee_setId(employee, (qtyEmployees+1)))
                    lista->add(lista, employee);
                qtyEmployees++;
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                seguir = 'n';
                break;
        }

        pauseScreen(); /**< Se pausa el iterador para mostrar el resultado en pantalla */
        printf("\n");
    }

    return 0;
}
