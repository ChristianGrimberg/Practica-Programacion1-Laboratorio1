#ifndef PROGRAMMENU_H_INCLUDED
#define PROGRAMMENU_H_INCLUDED

/** \brief
 * Funcion que limpia la pantalla de la consola
 * \param void Sin parametros
 * \return No retorna valores
 *
 */
void clearScreen(void);

/** \brief
 * Funcion que muestra el menu principal de opciones en consola y le pide la eleccion al usuario.
 * \param void Sin parametros.
 * \return Retorna el valor elegido por el usuario.
 *
 */
int mainMenu(void);

/** \brief
 * Funcion que pausa el programa en espera de que el usuario presione Enter.
 * \param void Sin parametros.
 * \return No retorna valores.
 *
 */
void pauseScreen(void);

/** \brief
 * Funcion que muestra el menu de borrado en consola y le pide la eleccion al usuario.
 * \param void No tiene parametros.
 * \return Devuelve la opcion de menu de borrado elegida por el usuario.
 *
 */
int deleteMenu(void);

#endif // PROGRAMMENU_H_INCLUDED
