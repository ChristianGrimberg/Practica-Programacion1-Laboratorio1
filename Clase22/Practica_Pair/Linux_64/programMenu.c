#include <stdio.h>
#include <stdlib.h>
#include "programMenu.h"
#include "validator.h"

void clearScreen(void)
{
    #ifdef __unix__
    {
        system("clear");
    }
    #else
    {
        system("cls");
    }
    #endif // __unix__
}

int mainMenu(void)
{
    int option;

    clearScreen();

    printf("======================================\n");
    printf("1. Parse del archivo data.csv\n");
    printf("2. Listar Empleados\n");
    printf("3. Ordenar por nombre\n");
    printf("4. Agregar un elemento\n");
    printf("5. Elimina un elemento\n");
    printf("6. Listar Empleados (Desde/Hasta)\n");
    printf("7. Salir del programa.\n");
    printf("======================================\n");

    option = requestNumberInRange(1, 7);

    return option;
}


void pauseScreen(void)
{
    printf("Presione la tecla Enter para continuar...");
    setbuf(stdin, NULL); /**< Se limpia el buffer de entrada del teclado para los distintos SO */
    getchar();
}
