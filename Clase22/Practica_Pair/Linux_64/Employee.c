#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ArrayList.h"
#include "Employee.h"
#include "validator.h"

int employee_compare(void* pEmployeeA,void* pEmployeeB)
{
    return 0;
}


void employee_print(Employee* this)
{
    /**< Impresion en pantalla de los datos del empleado */
    if(employee_getId(this)!=ERROR_VALUE)
        printf("Empleado: %d.\n", employee_getId(this));
}


Employee* employee_new(void)
{
    Employee* employeeAux = NULL;

    /**< Se reserva un nuevo espacio de memoria para almacenar el Empleado */
    employeeAux = (Employee*)malloc(sizeof(Employee));

    if(employeeAux==NULL)
    {
        printf("No hay espacio en memoria para un nuevo Empleado.\n");
    }

    return employeeAux;

}

void employee_delete(Employee* this)
{


}

int employee_setId(Employee* this, int id)
{
    int returnValue = 0;
    char numberChar[51];

    //itoa(id, numberChar, 10);
    sprintf( numberChar, "%d", id);

    if(isNumber(numberChar))
    {
        this->id = id;

        returnValue = 1;
    }

    return returnValue;

}

int employee_getId(Employee* this)
{
    int returnValue = ERROR_VALUE;
    char numberChar[51];

    //itoa(this->id, numberChar, 10);
    sprintf( numberChar, "%d", this->id);

    if(isNumber(numberChar))
        returnValue = this->id;

    return returnValue;
}


