#include <stdio.h>
#include <stdlib.h>
#include "ArrayList.h"
#include "Employee.h"
#include "parser.h"
#include "validator.h"

int parserEmployee(FILE* pFile , ArrayList* pArrayListEmployee)
{
    int returnValue = 0;
    /**< Variables estaticas para operar los valores obtenidos en cada linea del archivo */
    char idAux[51];
    char nameAux[51];
    char lastNameAux[51];
    char isEmptyAux[51];

    Employee* empleadoAux = employee_new(); /**< Se opera con un nuevo empleado en memoria dinamica */

    if(empleadoAux!=NULL)
    {
        while(!feof(pFile))
        {
            fscanf(pFile, "%[^,],%[^,],%[^,],%[^\n]\n", idAux, nameAux, lastNameAux, isEmptyAux);

            if(isNumber(idAux))
            {
                printf("debug: idAux-%d.\n", atoi(idAux));
                if(employee_setId(empleadoAux, atoi(idAux)))
                {
                    pArrayListEmployee->add(pArrayListEmployee, empleadoAux);
                    returnValue++;
                }
            }
            else
                continue;
        }
    }

    fclose(pFile);

    return returnValue;
}
