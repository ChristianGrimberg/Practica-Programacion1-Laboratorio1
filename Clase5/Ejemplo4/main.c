#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define CANT 5

int main()
{
    int v[CANT];
    int estado[CANT];
    int i;
    char seguir;

    for (i = 0; i < CANT; i++)
    {
        estado[i] = 0;
    }

    do
    {
        for (i = 0; i < CANT; i++)
        {
            if (estado[i] == 0)
            {
                printf("Ingrese el valor a cargar en el vector %d: ", i);
                scanf("%d", &v[i]);
                estado[i] = 1;
                break;
            }
        }
        printf("Desea ingresar otro dato S/N? ");
        setbuf(stdin, NULL);
        scanf("%c", &seguir);
        seguir = tolower(seguir);
    } while (seguir == 's');

    for (i = 0; i < CANT; i++)
    {
        if (estado[i] == 1)
        {
            printf("Posicion %d, valor %d\n", i, v[i]);
        }
    }

    return 0;
}
