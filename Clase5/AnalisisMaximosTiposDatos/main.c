#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

int main()
{
    printf("%d - %d - %d - %d\n", INT_MAX, INT_MAX - 1, INT_MAX + 1, INT_MAX + 50000);

    return 0;
}
