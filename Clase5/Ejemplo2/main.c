#include <stdio.h>
#include <stdlib.h>
#define CANT 5

int main()
{
    int i;
    int edad[CANT];
    float sueldo[CANT];
    int legajo[CANT];

    for (i = 0; i < CANT; i++)
    {
        legajo[i] = i + 1;
        printf("Ingrese la edad del legajo %d: ", legajo[i]);
        scanf("%d", &edad[i]);
        printf("Ingrese el sueldo del legajo %d: ", legajo[i]);
        scanf("%f", &sueldo[i]);
    }

    for (i = 0; i < CANT; i++)
    {
        printf("El legajo %d tiene %d anios y cobra $%.2f\n", legajo[i], edad[i], sueldo[i]);
    }

    return 0;
}
