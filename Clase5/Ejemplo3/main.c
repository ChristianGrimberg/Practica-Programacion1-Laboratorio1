#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define CANT 5

int main()
{
    int estado[CANT];
    int posicion = 0;
    char continuar = 's';
    int i;
    int j;

    for (i = 0; i < CANT; i++)
    {
        estado[i] = 0;
    }

    do
    {
        printf("Elegir posicion: ");
        scanf("%d", &posicion);

        if (estado[posicion] == 0)
        {
            printf("Ingrese el valor: ");
            scanf("%d", &estado[posicion]);
        }
        else
        {
            printf("La posicion ya esta ocupada\n");
        }

        printf("Desea seguir? s/n: ");
        setbuf(stdin, NULL);
        scanf("%c", &continuar);
        continuar = tolower(continuar);
    } while (continuar == 's');

    printf("Estados:\n");
    for (j = 0; j < CANT; j++)
    {
        printf("Posicion %d - %d\n", j, estado[j]);
    }

    return 0;
}
