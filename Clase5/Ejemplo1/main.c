#include <stdio.h>
#include <stdlib.h>
#define CANT 5

int main()
{
    int v[5], i;

    for (i = 0; i < 5; i++)
    {
        printf("Ingrese valor a cargar en el vector: ");
        scanf("%d", &v[i]);
    }

    printf("[");
    for (i = 0; i < 5; i++)
    {
        printf("%d ", v[i]);
    }
    printf("]");

    return 0;
}
