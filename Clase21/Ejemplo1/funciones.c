#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "funciones.h"

int size;
int index;
Persona** lista;

Persona* persona_newPersona(void)
{
    Persona* persona = (Persona*)malloc(sizeof(Persona));
    return persona;
}

int persona_setEdad(Persona* pPersona, int edad)
{
    int intReturn;
    if(edad>0)
    {
        pPersona->edad = edad;
        intReturn = 0; // OK
    }
    else
        intReturn = 1;

    return intReturn; // error
}

int persona_setName(Persona* pPersona, char* pName)
{
    int intReturn;
    if(strlen(pName)>3)
    {
        strcpy(pPersona->nombre,pName);
        intReturn = 0;
    }
    else
        intReturn = 1;
    return intReturn;
}

void persona_toString(Persona* pPersona)
{
    printf("Nombre:%s � Edad:%d", pPersona->nombre, pPersona->edad);
}

int persona_getEdad(Persona* pPersona)
{
    return pPersona->edad;
}

char* persona_getNombre(Persona* pPersona)
{
    return pPersona->nombre;
}

void persona_initLista(void)
{
    size = 10;
    index=0;

    lista = (Persona**)malloc(sizeof(Persona*)*size);
}

void persona_addPersona(Persona* p)
{
    lista[index]=p;
    index++;

    if(index>=size)
    {
        printf("no hay mas lugar, redefinimos el array\r\n");
        size=size+10;
        lista = (Persona**)realloc(lista,sizeof(Persona*)*size);
    }
}

void preguntarNombre(char nombre[])
{
    char nombreAux[20];
    printf("Ingrese el nombre: ");
    fflush(stdin);
    scanf("%s", nombreAux);
    strcpy(nombre, nombreAux);
}

int preguntarEdad(void)
{
    int edadAux;
    printf("Ingrese la edad: ");
    scanf("%d", &edadAux);

    return edadAux;
}

char preguntarSalir(void)
{
    char pregunta;
    printf("Desea salir (S/N)?");
    fflush(stdin);
    scanf("%c", &pregunta);

    pregunta = toupper(pregunta);

    return pregunta;
}
