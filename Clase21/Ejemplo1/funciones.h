#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

typedef struct
{
    int edad;
    char nombre[20];
} Persona;

Persona* persona_newPersona(void);

int persona_setEdad(Persona* pPersona, int edad);

int persona_setName(Persona* pPersona, char* pName);

void persona_toString(Persona* pPersona);

int persona_getEdad(Persona* pPersona);

char* persona_getNombre(Persona* pPersona);

void persona_initLista(void);

void persona_addPersona(Persona* p);

void preguntarNombre(char nombre[]);

int preguntarEdad(void);

char preguntarSalir(void);

#endif // FUNCIONES_H_INCLUDED
