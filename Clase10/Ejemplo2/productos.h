#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED
#define DESCRIPCION 51
#define CANTIDAD 101

typedef struct
{
    int IdProveedor;
    char descripcionProveedor[DESCRIPCION];
    int estadoProveedor;
} Proveedor;

typedef struct
{
    int IdProducto;
    char descripcionProducto[DESCRIPCION];
    float importeProducto;
    int cantidadProducto;
    int estadoProducto;
} Producto;

typedef struct
{
    int IdProducto;
    int IdProveedor;
} ProductoPorProveedor;

void ingresar(Producto producto[], int cantidadProducto);


#endif // FUNCIONES_H_INCLUDED
