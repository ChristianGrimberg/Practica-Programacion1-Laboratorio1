#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Declaracion e inicializacion de variables
    int i, j = 100, suma1 = 0, suma2 = 0;

    //Ejemplo de acumulacion con iterador for
    for (i = 0; i <= j; i++)
    {
        suma1 += i;
    }

    //Formula que simula acumulacion de un interador
    suma2 = j * (j + 1) / 2;

    //Se muestra en pantalla ambos resultados
    printf("%d - %d", suma1, suma2);

    return 0;
}
