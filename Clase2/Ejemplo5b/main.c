#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i
        ,numero
        , suma = 0
        , maximo
        , minimo;

    float promedio = 0;

    for (i = 0; i < 10; i++)
    {
        printf("Ingrese un numero: ");
        scanf("%d", &numero);

        suma += numero;

        if (i == 0)
        {
            maximo = numero;
            minimo = numero;
        }
        else
        {
            if (numero > maximo)
            {
                maximo = numero;
            }
            else
            {
                if (numero < minimo)
                {
                    minimo = numero;
                }
            }
        }
    }

    // Ejemplo de conversión de datos con CAST
    promedio = (float)suma / 10;
    printf("La suma es: %d\n", suma);
    printf("El promedio es: %.2f\n", promedio);
    printf("La maximo es: %d\n", maximo);
    printf("El minimo es: %d", minimo);

    return 0;
}
