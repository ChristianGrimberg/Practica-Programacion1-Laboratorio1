#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, j;

    do
    {
        printf("Ingrese un numero: ");
        scanf("%d", &i);
        printf("Ingrese otro numero: ");
        scanf("%d", &j);
    }
    while (i < j); //Es la unica estructura de control que al finalizar lleva ;

    return 0;
}
