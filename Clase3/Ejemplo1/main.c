//Aqu� se definen los archivos de cabecera
#include <stdio.h>
#include <stdlib.h>
//Aqu� se definen las constantes y macros

//Aqu� se declaran los prototipos que utiliza el programa
int sumar(int, int);

int main()
{
    //Declaracion e inicializacion de variables
    int x = 0
        ,y = 0
        ,respuesta = 0;

    printf("Ingrese un numero: ");
    scanf("%d", &x);
    printf("Ingrese otro numero: ");
    scanf("%d", &y);

    respuesta = sumar(x, y);

    printf("La suma de ambos es: %d\n", respuesta);

    return 0;
}

// �Que hace? �Que retorna? �Que Recibe?
int sumar(int numeroUno, int numeroDos)
{
    int resultado;

    resultado = numeroUno + numeroDos;

    return resultado;
}
