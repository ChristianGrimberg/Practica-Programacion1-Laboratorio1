#include <stdio.h>
#include <stdlib.h>

int factorial(int);
int iteracionFactorial(int);

int main()
{
    int valor, resultadoUno, resultadoDos;

    printf("\nIngrese numero: ");
    scanf("%d", &valor);

    resultadoUno = factorial(valor);
    resultadoDos = iteracionFactorial(valor);

    printf("\nEl factorial de %d es (%d <=> %d)", valor, resultadoUno, resultadoDos);

    return 0;
}

int factorial(int n)
{
    int resp;

    if(n == 1)
    {
       return 1;
    }
    resp = n* factorial(n-1);

    return(resp);
}

int iteracionFactorial(int numero)
{
    int contador;
    int resultado = 1;

    for (contador = 1; contador <= numero; contador++)
    {
        resultado = resultado * contador;
    }

    return resultado;
}
