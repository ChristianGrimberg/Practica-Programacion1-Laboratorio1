#include <stdio.h>
#include <stdlib.h>
#include "validacion.h"

int main()
{
    /**< Ejemplo 1: Saludador sin parametros y sin retorno */
    /*
    saludar();
    */

    /**< Ejemplo 2: Implementacion de validador de entero */
    /*
    int edad;
    int temperatura;

    edad = getInt("Ingrese una edad: ", "No es una edad", 1, 100, 1);
    temperatura = getInt("Ingrese temperatura: ", "No es una temperatura valida", 1, 90, 0);
    */

    /**< Ejemplo 3: Retorno devuelto de funciones, ejemplo scanf() */
    /*
    int numero;
    int respuestaScanf;

    printf("Ingrese un numero: ");
    respuestaScanf = scanf("%d", &numero);

    printf("Respuesta scanf(): %d\n", respuestaScanf);
    printf("Numero ingresado: %d", numero);
    */

    /**< Ejemplo 4: Uso de parametros por referencia */

    int numero = 3;
    cambiar(&numero);
    printf("Numero: %d", numero);


    /**< Ejemplo 5: Uso de funcion de evaluacion del valor ingresado con parametro por referencia*/
    /*
    int respuestaFuncion;
    int sueldo;

    respuestaFuncion = getIntEntreMinMax("Ingrese sueldo: ", 9000, 3000, &sueldo);

    if (respuestaFuncion == 0)
    {
        printf("Sueldo correcto: %d", sueldo);
    }
    else
    {
        if (respuestaFuncion < 0)
        {
            printf("Te falto");
        }
        else
        {
            printf("Te pasaste");
        }
    }
    */

    return 0;
}
