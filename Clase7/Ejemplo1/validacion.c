#include <stdio.h>
#include <stdlib.h>
#include "validacion.h"

int getInt(char mensaje[], char mensajeError[], int intentos, int maximo, int minimo)
{
    int retorno;

    do
    {
        printf("%s", mensaje);
        scanf("%d", &retorno);

        if (retorno <= maximo && retorno >= minimo)
        {
            break;
        }

        printf("%s\n", mensajeError);

        intentos--;

    } while (intentos > 0);

    return retorno;
}

void saludar(void)
{
    printf("Hola UTN FRA!");
}

int cambiar(int* dato)
{
    printf("Ingrese dato: ");
    fflush(stdin);
    scanf("%d", dato); /**< Devuelve el valor al parametro por referencia sin usar una variable auxiliar, sin "&" ni "*" */
    return 0;
}

int getIntEntreMinMax(char mensaje[], int maximo, int minimo, int* respuesta)
{
    int retorno;
    int ingresoAux;

    printf("%s", mensaje);
    scanf("%d", &ingresoAux);

    if (ingresoAux < minimo)
    {
        retorno = -1;
    }
    else
    {
        if (ingresoAux > maximo)
        {
            retorno = 1;
        }
        else
        {
            retorno = 0;
            *respuesta = ingresoAux;
        }
    }

    return retorno;
}
