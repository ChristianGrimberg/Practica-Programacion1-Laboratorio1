#ifndef VALIDACION_H_INCLUDED
#define VALIDACION_H_INCLUDED

/** \brief
 * getInt()
 * \param mensaje[] Mensaje de Pedido al usuario
 * \param mensajeError[] Mensaje de error
 * \param intentos Cantidad de posibilidades
 * \param maximo Maximo numero posible
 * \param minimo Minimo numero posible
 * \return Un valor entero valido
 *
 */
int getInt(char mensaje[], char mensajeError[], int intentos, int maximo, int minimo);

int getIntEntreMinMax(char mensaje[], int maximo, int minimo, int* respuesta);

/** \brief
 * saludar(void)
 * \param void Sin parametros
 * \return Sin retorno
 *
 */
void saludar(void); //Hola UTN FRA!

int cambiar(int* dato);

#endif
