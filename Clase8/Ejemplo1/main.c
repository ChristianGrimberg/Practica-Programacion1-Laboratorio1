#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int main()
{
    int edad;
    char cad[128];

    edad = 37;
    sprintf(cad, "%d", edad); /**< Se recomienda la funcion sprintf para validar datos ingresados */
    printf("%d\n", edad);
    printf("%s\n", cad);

    int numero;
    float flotante;
    numero = atoi(cad); /**< Caracter a numero */
    flotante = atof(cad); /**< Caracter a flotante */
    printf("%d - %.2f\n", numero, flotante);

    int esDigito;
    int digito;

    digito = 5;
    esDigito = isdigit(digito); /**< Devuelve 0 si no es un digito, y 1 si es digito */
    printf("%d\n", esDigito);

    int flag = 0;
    int i;
    for (i = 0; i < strlen(cad); i++)
    {
        if (isdigit(cad[i]) == 0)
        {
            flag = 1;
            break;
        }
    }

    printf("flag: %d\n", flag);

    int esAlfabeto;
    esAlfabeto = isalpha(digito); /**< Funcion para validar si el valor es letra del alfabeto, devuelve 1 si lo es, 0 si no es */
    printf("Alpha: %d\n", esAlfabeto);

    /**< Validar si es un entero o no */
    char prueba[128];
    int j, cantidad, bandera;

    bandera = 0;
    printf("Ingrese un valor: ");
    scanf("%s", prueba);
    cantidad = strlen(prueba);

    for (j = 0; j < cantidad; j++)
    {
        if (isdigit(prueba[j]) == 0)
        {
            bandera = 1;
            break;
        }
    }

    if (bandera == 0)
    {
        printf("Es un numero\n");
    }
    else
    {
        if (bandera == 1)
        {
            printf("No es un numero\n");
        }
    }


    return 0;
}
