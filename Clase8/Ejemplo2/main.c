#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "estructuras.h"

int main()
{
    struct sGente persona;

    printf("Ingrese el nombre: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]", persona.nombre);

    printf("->Fecha de Nacimiento<-\nIngrese el dia: ");
    scanf("%d", &persona.fechaNacimiento.dia);
    printf("Ingrese el mes: ");
    scanf("%d", &persona.fechaNacimiento.mes);
    printf("Ingrese el anio: ");
    scanf("%d", &persona.fechaNacimiento.anio);

    printf("Su nombre es %s y la Fecha de Nacimiento es %d/%d/%d", persona.nombre, persona.fechaNacimiento.dia, persona.fechaNacimiento.mes, persona.fechaNacimiento.anio);

    return 0;
}
