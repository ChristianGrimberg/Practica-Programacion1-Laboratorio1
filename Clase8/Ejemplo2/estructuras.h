#ifndef MENU_H_ESTRUCTURAS
#define MENU_H_ESTRUCTURAS

struct sFecha
{
    int dia;
    int mes;
    int anio;
};

struct sGente
{
    char nombre[31];
    struct sFecha fechaNacimiento;
};

#endif
