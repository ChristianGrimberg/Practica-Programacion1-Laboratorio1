# Memoria Dinamica
Funciones a utilizar:
* ```void* malloc(unsigned int);```
* ```calloc()```
* ```realloc()```
* ```free()```
* ```sizeOf()```

## Ejemplos en Clase
```c
int* ptr;
ptr = (int*) malloc(sizeOf(int)*10);
```
Si no encuentra lugar en la memoria, devuelve ```NULL```
